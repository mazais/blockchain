pragma solidity ^0.5.0;

contract Meme {
  
  struct F {
    string filename;
    string filehash;
  }

  F[] private files;

  function add(string memory filename, string memory filehash) public {
    F memory newFile = F(filename, filehash);
    files.push(newFile);
  }

  function length() public view returns(uint) {
    return files.length;
  }

  function getname(uint i) public view returns (string memory) {
    return files[i].filename;
  }

  function gethash(uint i) public view returns(string memory ) {
    return files[i].filehash;
  }
}
