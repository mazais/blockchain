import React, { Component } from 'react';
import Web3 from 'web3';
import './App.css';
import Meme from '../abis/Meme.json'

const ipfsClient = require('ipfs-http-client')
const ipfs = ipfsClient({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' }) // leaving out the arguments will default to these values

class App extends Component {

  async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }

  async loadBlockchainData() {
    const web3 = window.web3
    // Load account
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    const networkId = await web3.eth.net.getId()
    const networkData = Meme.networks[networkId]
    if(networkData) {
      const contract = web3.eth.Contract(Meme.abi, networkData.address)
      this.setState({ contract })
      const length = await contract.methods.length().call()
      for (let i=0; i < length; i++) {
        let file = {filename: '', filehash: ''}
        file.filename = await contract.methods.getname(i).call()
        file.filehash = await contract.methods.gethash(i).call()
        this.setState({
          files: [...this.state.files, file]
        })
      }
    } else {
      window.alert('Smart contract not deployed to detected network.')
    }
  }

  constructor(props) {
    super(props)

    this.state = {
      files: [],
      contract: null,
      web3: null,
      buffer: null,
      filename: null,
      account: null
    }
  }

  captureFile = (event) => {
    event.preventDefault()
    const file = event.target.files[0]
    const reader = new window.FileReader()
    reader.readAsArrayBuffer(file)
    reader.onloadend = () => {
      this.setState({ buffer: Buffer(reader.result) })
      console.log('buffer', this.state.buffer)
      this.setState({ filename: document.getElementById('file').files[0].name})
      console.log('name', this.state.filename)
    }
  }

  onSubmit = (event) => {
    event.preventDefault()
    console.log("Submitting file to ipfs...")

    /*
    ipfs.add(this.state.buffer, (error, result) => {
      console.log('Ipfs result', result)
      if(error) {
        console.error(error)
        return
      }
       this.state.contract.methods.set(result[0].hash).send({ from: this.state.account }).then((r) => {
         return this.setState({ memeHash: result[0].hash })
       })
    })
    */

    ipfs.add(this.state.buffer).then((result) => {
      console.log("hey")
        this.state.contract.methods.add(this.state.filename,result[0].hash).send({ from: this.state.account }, (() => {
          let file = {filename: '', filehash: ''}
          file.filename = this.state.filename
          file.filehash = result[0].hash;
          this.setState({
            files: [...this.state.files, file]
          })
        }));
    })
  }

  render() {
    const items = []

    for (const [index, value] of this.state.files.entries()) {
      items.push(
      <li key={index}>
        <a href={`https://ipfs.infura.io/ipfs/${value.filehash}`}>{value.filename}</a>
      </li>)
    }

    return (
      <div>
        <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
   
        </nav>
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 d-flex text-center">
              <div className="content mr-auto ml-auto">
                <ul>
                  {items}
                </ul>
                <p>&nbsp;</p>
                <h2>Add File</h2>
                <form onSubmit={this.onSubmit} >
                  <input type='file' id='file' onChange={this.captureFile} />
                  <input type='submit' />
                </form>
              </div>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
